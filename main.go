package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"math"
	"os"
	"os/exec"
	"strings"
)

func getSideLength(numberOfPixels int) int {
	return int(math.Ceil(math.Sqrt(float64(numberOfPixels))))
}

type RawToImage interface {
	Convert(io.Reader) (*image.RGBA, error)
	Unconvert(io.Writer, *image.RGBA) error
}

type F32RawToImage struct{}

func (q *F32RawToImage) Convert(r io.Reader) (*image.RGBA, error) {
	var buf = &bytes.Buffer{}

	if _, err := io.Copy(buf, r); err != nil {
		return nil, err
	}

	var dataLength = buf.Len()

	if dataLength%4 != 0 {
		return nil, fmt.Errorf("length in bytes not divisible by 4")
	}

	numberOfPixels := dataLength / 4
	pixelDataLength := dataLength
	sideLength := getSideLength(numberOfPixels)

	img := image.NewRGBA(image.Rectangle{
		Max: image.Pt(sideLength, sideLength),
	})

	for i := 0; i < pixelDataLength; {
		var v float32
		if err := binary.Read(buf, binary.BigEndian, &v); err != nil {
			break
		}

		vv := uint8(((v * 0.5) + 0.5) * 255.0)

		img.Pix[i+0] = vv
		img.Pix[i+1] = vv
		img.Pix[i+2] = vv
		img.Pix[i+3] = 0xFF

		i += 4
	}

	return img, nil
}

func (q *F32RawToImage) Unconvert(w io.Writer, img *image.RGBA) error {
	pixelDataLength := len(img.Pix)

	for i := 0; i < pixelDataLength; {
		// red channel
		v := (float32(img.Pix[i])/255.0)*2.0 - 1.0
		if err := binary.Write(w, binary.BigEndian, v); err != nil {
			break
		}
		i += 4
	}

	return nil
}

func main() {
	var img *image.RGBA
	var conv RawToImage = new(F32RawToImage)

	audioFilePath := os.Args[1]
	goodRawAudioFilePath := os.Args[2]
	goodRawAudioImageFilePath := os.Args[3]
	borkedRawAudioFilePath := os.Args[4]
	borkedAudioFilePath := os.Args[5]

	log.Println(strings.Join([]string{
		audioFilePath,
		goodRawAudioFilePath,
		goodRawAudioImageFilePath,
		borkedRawAudioFilePath,
		borkedAudioFilePath,
	}, "\n"))

	log.Println("converting audio to raw")
	ffmpeg := exec.Cmd{
		Path: "/usr/bin/ffmpeg",
		Args: []string{
			"-nostdin",
			"-y",
			"-i",
			audioFilePath,
			"-f", "f32be",
			"-ac", "1",
			goodRawAudioFilePath,
		},
	}

	if err := ffmpeg.Run(); err != nil {
		log.Fatal(err)
	}

	log.Println("converting raw to image")
	if fd, err := os.Open(goodRawAudioFilePath); err != nil {
		log.Fatal(err)
	} else {
		if img, err = conv.Convert(fd); err != nil {
			log.Fatal(err)
		}
	}

	log.Println("saving image")
	if fd, err := os.Create(goodRawAudioImageFilePath); err != nil {
		log.Fatal(err)
	} else {
		png.Encode(fd, img)
		fd.Close()
	}

	buf := &bytes.Buffer{}

	log.Println("encoding lossy")
	if err := jpeg.Encode(buf, img, &jpeg.Options{Quality: 90}); err != nil {
		log.Fatal(err)
	}

	log.Println("decoding lossy")
	if jpgImage, err := jpeg.Decode(buf); err != nil {
		log.Fatal(err)
	} else {
		for y := 0; y < jpgImage.Bounds().Dy(); y++ {
			for x := 0; x < jpgImage.Bounds().Dx(); x++ {
				img.Set(x, y, jpgImage.At(x, y))
			}
		}
	}

	log.Println("saving borked raw output")
	if fd, err := os.Create(borkedRawAudioFilePath); err != nil {
		log.Fatal(err)
	} else {
		err := conv.Unconvert(fd, img)
		fd.Close()
		if err != nil {
			log.Fatal(err)
		}
	}

	log.Println("converting borked raw to audio")
	ffmpeg = exec.Cmd{
		Path: "/usr/bin/ffmpeg",
		Args: []string{
			"-nostdin",
			"-y",
			"-f", "f32be",
			"-ar", "48000",
			"-ac", "1",
			"-i", borkedRawAudioFilePath,
			borkedAudioFilePath,
		},
	}

	if err := ffmpeg.Run(); err != nil {
		log.Fatal(err)
	}
}
